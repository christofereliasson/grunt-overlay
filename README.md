grunt-overlay
=============

Grunt overlay task - used for copying files over an existing directory structure.

## Getting Started
Install the grunt-overlay plugin by adding the latest grunt overlay
release to your devDependencies list in your package.json file. Then
resolve the dependency by performing `npm install` on the command line.

This will download the dependencies listed in the package.json (please
read the [npm documentation](https://npmjs.org) for more details on
this.)

You will also require the [grunt](http://gruntjs.com)
application to be installed (perferably globally) this can be done by
entering `npm install -g grunt-cli`.

Finally you will need to create a `Gruntfile.js` file from which grunt will
use as a reference point for project configuration. (see the [example grunt.js](
https://bitbucket.org/foldr/grunt-overlay/src/master/test/grunt.js) for a
starting point as to how to use the grunt-overlay task.

## Configuration Options
The grunt-overlay task is a grunt *multitask* meaning that it is
assocaited with a number of development profiles. In the following
grunt.js initContfig snippet the profile is 'javascript':
```javascript
	...
	overlay: {
		javascript: {
			src: ['src/**/*.js'], // source file search path
			dest: 'dest/js',      // the destination folder
			options: {            // options can be omitted
				overwrite: true,  // overwrite existing files (default to false)
				base_path: 'src'  // path segment to ignore in src (default is empty
                                  // or the directories prior to '**' if found).
			}
		}
	}

	...
```

---
Copyright (C) 2015 Benjamin Conlan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
